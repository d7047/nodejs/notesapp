const fs = require('fs')
const validator = require('validator')
const notes1 = require('./notes1.js')
//const notes = require('./notes.js')
const color = require('chalk')
const command = process.argv[2]
const yargs = require('yargs')
const { string } = require('yargs')
const { addNote } = require('./notes1.js')
//fs.writeFileSync('notes.txt', 'this file is written by Nodejs!')

//fs.appendFileSync('notes.txt', 'data added by appendfileasync')
// const msg = notes1()
// console.log(msg)
// console.log(validator.isEmail('sarveshsawant@gmail.com'))
// console.log(validator.isURL('https://www.vk18.com'))
// console.log(color.green('success'))
// console.log(color.red.bold('RCB '))
// console.log(color.green('sarvesh '))
// console.log(process.argv[2])
// if (command === 'add') {
//   console.log('adding a note')
// } else if (command === 'remove') {
//   console.log('remove note')
// }

//console.log(process.argv)
//yargs.version('1.1.0')

//creating add command
yargs.command({
  command: 'add',
  describe: 'add a new note',
  builder: {
    title: {
      describe: 'Note Title',
      demandOption: true, // if we want given proeprty or option to be always present while exceuting command// bydefault its false (its just like a required function)
      type: 'string', // data type acccepted.we want title proerty always string ,othereise if we run node app.js --title ,property will come as boolean
    },
    body: {
      describe: 'add a Body for a note',
      demandOption: true,
      type: 'string',
    },
  },
  handler(argv) {
    notes1.addNote(argv.title, argv.body)
  },
})
//creating remove command
yargs.command({
  command: 'remove',
  describe: 'remove a note',
  builder: {
    title: {
      describe: 'Note Title',
      demandOption: true, // if we want given proeprty or option to be always present while exceuting command// bydefault its false (its just like a required function)
      type: 'string', // data type acccepted.we want title proerty always string ,othereise if we run node app.js --title ,property will come as boolean
    },
  },
  handler(argv) {
    notes1.removeNote(argv.title)
  },
})
//creating list command
yargs.command({
  command: 'list',
  describe: 'Listing all the notes',
  handler() {
    notes1.listnote()
  },
})
//creating read command
yargs.command({
  command: 'read',
  describe: 'reading a note',
  builder: {
    title: {
      describe: 'which note you want to read please put to title',
      demandOption: true, // if we want given proeprty or option to be always present while exceuting command// bydefault its false (its just like a required function)
      type: 'string', // data type acccepted.we want title proerty always string ,othereise if we run node app.js --title ,property will come as boolean
    },
  },
  handler(argv) {
    notes1.readnote(argv.title)
  },
})

yargs.parse() // it parses the commands depedning upon configuration provided with yargs command
