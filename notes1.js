const fs = require('fs')

const chalk = require('chalk')

const addNote = (title, body) => {
  const loadNote = loadfile()
  const duplicate = loadNote.filter((note) => note.title === title)
  const duplicates = loadNote.find((note) => note.title === title)
  if (!duplicates) {
    loadNote.push({
      title: title,
      body: body,
    })
    savnote(loadNote)
    console.log(chalk.green.inverse('new note addded...'))
  } else {
    console.log(
      chalk.red.inverse(
        'Note with same Title alreday present please change title'
      )
    )
  }
}

const loadfile = () => {
  try {
    const databuffer = fs.readFileSync('Notes.json')
    const stringdata = databuffer.toString()
    return JSON.parse(stringdata)
  } catch (e) {
    return []
  }
}

const removeNote = (title) => {
  const Notes = loadfile()
  const notestokeep = Notes.filter((note) => {
    if (note.title === title) {
      console.log(chalk.green.inverse('note with Title ' + title + 'Removed'))
    }
    return note.title !== title
  })
  if (notestokeep.length == Notes.length) {
    console.log(chalk.red.inverse('No Note removed as title was not matching'))
  } else {
    savnote(notestokeep)
  }
}
debugger
const listnote = () => {
  console.log(chalk.inverse('Your Notes :'))
  const loadNote = loadfile()
  loadNote.forEach((element) => {
    console.log(chalk.yellow(element.title))
  })
}
const savnote = (Notes) => {
  const datajson = JSON.stringify(Notes)
  fs.writeFileSync('Notes.json', datajson)
}

const readnote = (title) => {
  const loadnotes = loadfile()
  const found = loadnotes.find((note) => note.title === title)
  if (found) {
    console.log(chalk.inverse('Title :') + title)
    console.log(chalk.inverse('Body :') + found.body)
  } else {
    console.log(
      chalk.inverse.red('No note with title ' + title + ' found to read')
    )
  }
}
module.exports = {
  addNote: addNote,
  removeNote: removeNote,
  listnote: listnote,
  readnote: readnote,
}
